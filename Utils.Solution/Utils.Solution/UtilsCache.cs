using System;
using System.IO;
using System.Net;

namespace Utils.Solution
{
	public static class UtilsCache
	{
		private const string UtilUrlFormat = "https://bitbucket.org/dotnetutils/utils-sln-{0}/get/HEAD.zip";

		public static string GetUtilPath(string utilName)
		{
			var cachedUtilPath = CachedUtilPath(utilName);

			var url = string.Format(UtilUrlFormat, utilName);

			var file = new FileInfo(cachedUtilPath);

			try
			{
				var request = WebRequest.Create(new Uri(string.Format(UtilUrlFormat, utilName)));
				request.Method = "HEAD";

				using (var response = request.GetResponse())
				{
					var contentLength = response.ContentLength;

					if (!file.Exists || file.Length != contentLength)
					{
						if (!file.Directory.Exists) file.Directory.Create();

						using (var wc = new WebClient())
						{
							var tmpFilePath = file.FullName + ".tmp";
							if (File.Exists(tmpFilePath)) File.Delete(tmpFilePath);

							wc.DownloadFile(url, tmpFilePath);

							if (file.Exists) file.Delete();
							File.Move(tmpFilePath, file.FullName);

							return file.FullName;
						}
					}
				}
			}
			catch (WebException)
			{
				// It's ok if we can't download the file
			}

			return file.Exists ? file.FullName : null;
		}

		private static string CachedUtilPath(string utilName)
		{
			var cachedUtilPath = Path.Combine(
				Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData),
				"SlnUtils",
				utilName + ".zip");
			return cachedUtilPath;
		}
	}
}