﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;

namespace Utils.Solution
{
	class Program
	{
		const string SlnDir = ".sln";

		static void Main(string[] args)
		{
			if (args.Length < 2)
			{
				ConsoleWriteHelp();
				return;
			}

			var verb = args[0].ToLowerInvariant();
			var name = args[1].ToLowerInvariant();
			var itemsToSkip = 2;

			if (verb == "init" && name == "--create")
			{
				if (args.Length < 3)
				{
					ConsoleWriteHelp();
					return;
				}

				itemsToSkip++;
				verb = "init-create";
				name = args[2].ToLowerInvariant();
			}

			var toolArgs = args.Skip(itemsToSkip).ToArray();
			var workingDir = Path.Combine(SlnDir, name);

			if (!Directory.Exists(SlnDir)) Directory.CreateDirectory(SlnDir);

			switch (verb)
			{
				case "init":
				case "init-create":
					if (Directory.Exists(workingDir))
					{
						Console.WriteLine("The template is already initialized. Use \"sln clean {0}\" to remove.", name);
						return;
					}
					var utilPath = UtilsCache.GetUtilPath(name);
					if (utilPath == null)
					{
						Console.WriteLine("Template \"{0}\" is not found.", name);
						return;
					}

					var root = Directory.GetDirectoryRoot(workingDir);
					var tmpUnzip = Path.Combine(root, "~sln");

					if (Directory.Exists(tmpUnzip)) Directory.Delete(tmpUnzip, true);
					System.IO.Compression.ZipFile.ExtractToDirectory(utilPath, tmpUnzip);
					var unzipped = Directory.GetDirectories(tmpUnzip).First();
					Directory.Move(unzipped, workingDir);
					if (Directory.Exists(tmpUnzip)) Directory.Delete(tmpUnzip, true);

					Execute(name, "Initializer", toolArgs);
					if (verb == "init-create")
					{
						Execute(name, "Creator", toolArgs);

						Execute(name, "Cleaner", toolArgs);

						Process.Start(new ProcessStartInfo
						{
							Arguments = "/C timeout 3 & rd /s /q .sln",
							WindowStyle = ProcessWindowStyle.Hidden,
							CreateNoWindow = true,
							FileName = "cmd.exe"
						});
					}

					break;
				case "create":
					if (!Directory.Exists(workingDir))
					{
						Console.WriteLine("The template is not initialized. Use \"sln init {0}\"", name);
						return;
					}

					Execute(name, "Creator", toolArgs);

					Execute(name, "Cleaner", toolArgs);

					Process.Start(new ProcessStartInfo
					{
						Arguments = "/C timeout 3 & rd /s /q .sln",
						WindowStyle = ProcessWindowStyle.Hidden,
						CreateNoWindow = true,
						FileName = "cmd.exe"
					});
					break;
				case "clean":
					if (!Directory.Exists(workingDir))
					{
						Console.WriteLine("The template is not initialized. Use \"sln init {0}\"", name);
						return;
					}

					Execute(name, "Cleaner", toolArgs);

					Process.Start(new ProcessStartInfo
					{
						Arguments = "/C timeout 1 & rd /s /q .sln",
						WindowStyle = ProcessWindowStyle.Hidden,
						CreateNoWindow = true,
						FileName = "cmd.exe"
					});

					break;
			}
		}

		private static void ConsoleWriteHelp()
		{
			Console.WriteLine("Usage:");
			Console.WriteLine("    sln init          {template} [init arg1 arg2]");
			Console.WriteLine("    sln init --create {template} [init arg1 arg2]");
			Console.WriteLine("    sln create        {template} [create arg1 arg2]");
			Console.WriteLine("    sln clean         {template} [cleanarg1 arg2]");
		}

		private static void Execute(string toolName, string action, string[] args)
		{
			var dllPath = Path.Combine(SlnDir, toolName, @"Executable", "Executable.exe");
			if (!File.Exists(dllPath))
			{
				Console.WriteLine("Can't find Executable file in template");
				return;
			}

			var assembly = Assembly.LoadFrom(dllPath);

			var cleaner = assembly.DefinedTypes.SingleOrDefault(x => x.Name == action);
			if (cleaner != null)
			{
				var executeMethod = cleaner.GetMethod("Execute");
				if (executeMethod != null)
				{
					var parameters = executeMethod.GetParameters();
					if (parameters.Length == 1 && parameters[0].ParameterType == typeof(string[]))
					{
						executeMethod.Invoke(null, new object[] { args });
					}
				}
			}
		}
	}
}
