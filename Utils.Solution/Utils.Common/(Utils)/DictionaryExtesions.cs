using System.Collections.Generic;

namespace Utils.Solution.Xamarin
{
	public static class DictionaryExtesions
	{
		public static TValue ValueOrDefault<TKey, TValue>(this Dictionary<TKey, TValue> dictionary, TKey key)
		{
			if (dictionary.ContainsKey(key))
			{
				return dictionary[key];
			}

			return default(TValue);
		}
	}
}