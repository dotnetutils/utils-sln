using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Utils.Solution;

namespace Utils.Common
{
	public static class Do
	{
		public static void Safe(Action action, int times = 5)
		{
			while (times-- > 0)
			{
				try
				{
					action();
					return;
				}
				catch
				{
					// ignored
				}
			}
		}
	}

	public static class ReplaceUtil
	{
		private const string DefaultExtensions = "*.sln;*.cs;*.csproj;*.config;*.plist;*.properties;*.resx;*.txt;*.xaml;*.axml;*.xml;*.js;*.css";

		public static void Replace(string currentDirectory, string oldValue, string newValue, string exts = null)
		{
			var extensions = (exts ?? DefaultExtensions).Split(';')
				.Where(x => !string.IsNullOrWhiteSpace(x))
				.Select(x => x.WildcardToRegex());

			foreach (var sourceFileName in Directory
				.GetFiles(currentDirectory, "*", SearchOption.AllDirectories)
				.Where(x => extensions.Any(c => Regex.IsMatch(x, c)))
				.Select(x => x.Substring(currentDirectory.Length + 1)))
			{
				ReplaceInContent(Path.Combine(currentDirectory, sourceFileName), oldValue, newValue);
				var fileName = Path.GetFileName(sourceFileName);
				var directoryName = Path.GetDirectoryName(sourceFileName);
				if (fileName != null && fileName.Contains(oldValue))
				{
					var destFileName = Path.Combine(currentDirectory, directoryName, fileName.Replace(oldValue, newValue));
					Do.Safe(() => File.Move(Path.Combine(currentDirectory, sourceFileName), destFileName));
				}
			}

			var stack = new Stack<string>();
			stack.Push(currentDirectory);

			while (stack.Any())
			{
				foreach (var directory in Directory.GetDirectories(stack.Pop()))
				{
					var fileName = Path.GetFileName(directory);
					var directoryName = Path.GetDirectoryName(directory);

					if (fileName != null && fileName.Contains(oldValue))
					{
						var destDirName = Path.Combine(directoryName, fileName.Replace(oldValue, newValue));
						Do.Safe(() => Directory.Move(directory, destDirName));
						stack.Push(destDirName);
					}
					else
					{
						stack.Push(directory);
					}
				}
			}
		}

		public static void ReplaceInContent(string sourceFileName, string oldValue, string newValue)
		{
			var allText = File.ReadAllText(sourceFileName, Encoding.UTF8);
			if (allText.Contains(oldValue))
				File.WriteAllText(sourceFileName, allText.Replace(oldValue, newValue), Encoding.UTF8);
		}
	}
}