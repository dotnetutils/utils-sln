using System;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;

namespace Utils.Solution
{
	public static class StringExtensions
	{
		public static string WildcardToRegex(this string wildCard)
		{
			return "^" + Regex.Escape(wildCard).Replace("\\*", ".*").Replace("\\?", ".") + "$";
		}

		public static string[] AsParts(this string line)
		{
			if (string.IsNullOrWhiteSpace(line))
			{
				return null;
			}

			return line
				.ToLowerInvariant()
				.Split(new[] { ' ', '\t' }, StringSplitOptions.RemoveEmptyEntries)
				.Select(x => x.Trim())
				.ToArray();
		}
		public static string[] AsTitleParts(this string line, string[] abbriviations = null)
		{
			var textInfo = new CultureInfo("en-US", false).TextInfo;

			if (string.IsNullOrWhiteSpace(line))
			{
				return null;
			}

			return line
				.Split(new[] { ' ', '\t' }, StringSplitOptions.RemoveEmptyEntries)
				.Select(x => x.Trim())
				.Select(x => abbriviations != null && abbriviations.Any(a => string.Equals(a, x, StringComparison.OrdinalIgnoreCase))
					? x
					: textInfo.ToTitleCase(x.ToLowerInvariant()))
				.ToArray();
		}

		public static string Joined(this string[] lines, string separator = null)
		{
			if (lines == null)
			{
				return null;
			}

			if (separator == null)
			{
				return string.Concat(lines);
			}

			return string.Join(separator, lines);
		}
	}
}