﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace Utils.Common
{
	public static class ImageUtil
	{
		public static void Resize(string inputPath, string outputPath, int newHeight, int newWidth)
		{
			using (var image = Image.FromFile(inputPath))
			using (var resized = ResizeInternal(image, newHeight, newWidth))
			{
				resized.Save(outputPath);
			}
		}

		private static Image ResizeInternal(Image image, int newHeight, int newWidth)
		{
			if ((image.Height == newHeight) && (image.Width == newWidth))
			{
				return image.Clone() as Image;
			}

			if (Math.Abs(image.Height - newWidth) == Math.Abs(newHeight - image.Width))
			{
				return BaseResize(image, newHeight, newWidth);
			}

			var k = image.Height / (float)image.Width;
			var width = (int)(newHeight / k);

			if (width > newWidth)
			{
				var outputImage = FitToHeight(image, newHeight);

				var indent = (outputImage.Width - newWidth) / 2;
				var selection = new Rectangle(indent, 0, newWidth, newHeight);
				return Crop(outputImage, selection);
			}
			else
			{
				var outputImage = FitToWidth(image, newWidth);

				var indent = Math.Abs((outputImage.Height - newHeight) / 2);
				var selection = new Rectangle(0, indent, newWidth, newHeight);
				return Crop(outputImage, selection);
			}
		}

		private static Image FitToHeight(Image image, int newHeight)
		{
			if (image == null)
			{
				throw new ArgumentNullException("image");
			}

			var k = image.Height / (float)image.Width;
			var newWidth = (int)(newHeight / k);

			return BaseResize(image, newHeight, newWidth);
		}

		private static Image FitToWidth(Image image, int newWidth)
		{
			if (image == null)
			{
				throw new ArgumentNullException("image");
			}

			var k = image.Height / (float)image.Width;
			var newHeight = (int)(newWidth * k);

			return BaseResize(image, newHeight, newWidth);
		}

		private static Image Crop(Image image, Rectangle selection)
		{
			var bitmap = image as Bitmap;
			return Crop(bitmap, selection);
		}

		private static Bitmap Crop(Bitmap image, Rectangle selection)
		{
			return image.Clone(selection, image.PixelFormat);
		}

		private static Bitmap BaseResize(Image image, int newHeight, int newWidth)
		{
			var outputImage = new Bitmap(newWidth, newHeight);

			using (var gr = Graphics.FromImage(outputImage))
			{
				var imageRectangle = new Rectangle(0, 0, newWidth, newHeight);

				gr.InterpolationMode = InterpolationMode.High;
				gr.CompositingQuality = CompositingQuality.HighQuality;
				gr.PixelOffsetMode = PixelOffsetMode.HighQuality;
				gr.SmoothingMode = SmoothingMode.AntiAlias;
				gr.DrawImage(image, imageRectangle);
			}

			return outputImage;
		}
	}
}