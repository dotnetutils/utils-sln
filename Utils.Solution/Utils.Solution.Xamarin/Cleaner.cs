using System;
using System.IO;

namespace Utils.Solution.Xamarin
{
	public class Cleaner
	{
		public static void Execute(string[] args = null)
		{
			File.Delete(Program.ConfigFile);
			Console.WriteLine("Xamarin solution is cleaned");
		}
	}
}