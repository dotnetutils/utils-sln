using System;
using System.IO;
using System.Linq;
using Utils.Common;

namespace Utils.Solution.Xamarin
{
	public class Creator
	{
		public static void Execute(string[] args = null)
		{
			var values = File.ReadAllLines(Program.ConfigFile)
				.Select(x => x.Trim())
				.Where(x => !string.IsNullOrWhiteSpace(x) && !x.StartsWith("#"))
				.ToDictionary(x => x.Split(':')[0].Trim(), x => x.Split(':')[1].Trim());

			var templatePath = Path.Combine(Program.ToolDirectory, "SolutionTemplate");

			foreach (var file in Directory.GetDirectories(templatePath))
			{
				var relative = file.Substring(templatePath.Length + 1);
				var destFileName = Path.Combine(Program.CurrentDirectory, relative);
				Directory.Move(file, destFileName);
			}
			foreach (var file in Directory.GetFiles(templatePath))
			{
				var relative = file.Substring(templatePath.Length + 1);
				var destFileName = Path.Combine(Program.CurrentDirectory, relative);
				File.Move(file, destFileName);
			}

			templatePath = Program.CurrentDirectory;

			var saveCaseConfig = values.ValueOrDefault("Save Case");

			var saveCaseItems = (saveCaseConfig ?? string.Empty)
				.Split(',').Select(x => x.Trim()).ToArray();

			var productNameAsIs = values.ValueOrDefault("Product Name");
			var productNameLowerCase = productNameAsIs.AsParts().Joined(".");

			var companyNameAsIs = values.ValueOrDefault("Company Name");
			var companyNameLowerCase = companyNameAsIs.AsParts().Joined(".");

			var packageId = companyNameLowerCase == null
				? string.Join(".", "com", productNameLowerCase)
				: string.Join(".", "com", companyNameLowerCase, productNameLowerCase);

			var companyNameTitleCase = companyNameAsIs.AsTitleParts(saveCaseItems).Joined(".");
			var productNameTitleCase = productNameAsIs.AsTitleParts(saveCaseItems).Joined(".");

			// Droid
			ReplaceUtil.ReplaceInContent(
				Path.Combine(templatePath, @"CompanyName.ProductName.Droid\SplashActivity.cs"),
				"droid-Product Name",
				values.ValueOrDefault("Droid Application Label") ?? productNameAsIs);
			ReplaceUtil.ReplaceInContent(
				Path.Combine(templatePath, @"CompanyName.ProductName.Droid\Properties\AndroidManifest.xml"),
				"com.droid.companyname.droid.productname",
				values.ValueOrDefault("Droid Package Id") ?? packageId);
			ReplaceUtil.ReplaceInContent(
				Path.Combine(templatePath, @"CompanyName.ProductName.Droid\Properties\AndroidManifest.xml"),
				"droid-Product Name",
				values.ValueOrDefault("Droid Application Label") ?? productNameAsIs);

			// iOS
			ReplaceUtil.ReplaceInContent(
				Path.Combine(templatePath, @"CompanyName.ProductName.iOS\Info.plist"),
				"ios-Product Name",
				values.ValueOrDefault("iOS CFBundleDisplayName") ?? productNameAsIs);
			ReplaceUtil.ReplaceInContent(
				Path.Combine(templatePath, @"CompanyName.ProductName.iOS\Info.plist"),
				"com.ios-companyname.ios-productname",
				values.ValueOrDefault("iOS CFBundleIdentifier") ?? packageId);

			// Windows Phone
			ReplaceUtil.ReplaceInContent(
				Path.Combine(templatePath, @"CompanyName.ProductName.WinPhone\Package.appxmanifest"),
				"wp-Company Name",
				values.ValueOrDefault("WindowsPhone Publisher") ?? companyNameAsIs ?? "Publisher Name");
			ReplaceUtil.ReplaceInContent(
				Path.Combine(templatePath, @"CompanyName.ProductName.WinPhone\Package.appxmanifest"),
				"wp-Product Name",
				values.ValueOrDefault("WindowsPhone Application") ?? productNameAsIs);
			ReplaceUtil.ReplaceInContent(
				Path.Combine(templatePath, @"CompanyName.ProductName.WinPhone\Package.appxmanifest"),
				"com.wp-companyname.wp-productname",
				values.ValueOrDefault("WindowsPhone Package Id") ?? packageId);

			var fullProductName = companyNameTitleCase == null
				? productNameTitleCase
				: string.Concat(companyNameTitleCase, ".", productNameTitleCase);

			ReplaceUtil.Replace(templatePath,
				"CompanyName.ProductName",
				fullProductName);

			var guidLength = Guid.Empty.ToString("B").Length;
			var guids = Directory
				.GetFiles(templatePath, "*.csproj", SearchOption.AllDirectories)
				.Select(csproj =>
				{
					var guidLine = File.ReadAllLines(csproj)
						.Single(x => x.Trim().StartsWith("<ProjectGuid>"));
					return guidLine.Substring(guidLine.IndexOf("{"), guidLength);
				})
				.ToArray();

			foreach (var guid in guids)
			{
				ReplaceUtil.Replace(templatePath,
					guid,
					Guid.NewGuid().ToString("B").ToUpperInvariant(),
					"*.csproj;*.sln");
			}

			Console.WriteLine("Xamarin solution is created");
		}
	}
}
