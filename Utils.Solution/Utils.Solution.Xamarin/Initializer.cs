using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using Utils.Common;

namespace Utils.Solution.Xamarin
{
	public class Initializer
	{
		private static readonly Dictionary<string[], Action<string>> Updaters = new Dictionary<string[], Action<string>>
			{
				{new []{"pn", "productName"}, UpdateProductName},
				{new []{"cn", "companyName"}, UpdateCompanyName},
				{new []{"sc", "saveCase"}, UpdateSaveCase},
			};

		public static void Execute(string[] args)
		{
			File.Copy(
				Path.Combine(Program.ToolDirectory, "InitContent", Program.ConfigFile),
				Program.ConfigFile);

			if (args.All(x => x.Contains("=")))
			{
				for (int i = 0; i < args.Length; i++)
				{
					var strings = args[i].Split('=');

					var paramKey = strings[0];
					var paramVal = strings[1];

					Update(paramKey, paramVal);
				}
			}
			else if (args.All(x => !x.Contains("=")))
			{
				if (args.Length > 0)
				{
					UpdateProductName(args[0]);
				}
				if (args.Length > 1)
				{
					UpdateCompanyName(args[1]);
				}
				if (args.Length > 2)
				{
					UpdateSaveCase(args[2]);
				}
			}

			Console.WriteLine("Xamarin solution is initialized");
		}

		private static void Update(string key, string value)
		{
			var updaterKey = Updaters.Keys
				.SingleOrDefault(updater => updater
					.Any(x => string.Equals(x, key, StringComparison.InvariantCultureIgnoreCase)));

			if (updaterKey != null)
			{
				Updaters[updaterKey](value);
			}

		}

		private static void UpdateSaveCase(string abbriviations)
		{
			ReplaceUtil.ReplaceInContent(
				Program.ConfigFile,
				"# Abbriviations: MSBuild, NUnit, BBC",
				"Abbriviations: " + abbriviations);
		}

		private static void UpdateCompanyName(string companyName)
		{
			ReplaceUtil.ReplaceInContent(
				Program.ConfigFile,
				"# Company Name: Your company name",
				"Company Name: " + companyName);
		}

		private static void UpdateProductName(string productName)
		{
			ReplaceUtil.ReplaceInContent(
				Program.ConfigFile,
				"Product Name: Your product name",
				"Product Name: " + productName);
		}
	}
}