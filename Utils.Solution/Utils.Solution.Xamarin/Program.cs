﻿using System.IO;

namespace Utils.Solution.Xamarin
{
	public static class Program
	{
		public static string CurrentDirectory { get { return Directory.GetCurrentDirectory(); } }
		public static string ToolDirectory { get { return Path.Combine(CurrentDirectory, ".sln", "xamarin"); } }

		public const string ConfigFile = "config.yml";

		public static void Main(string[] args)
		{
			// Initializer.Execute(args);
			// Creator.Execute(args);
			// Cleaner.Execute(args);
		}
	}
}
